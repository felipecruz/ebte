#include <stdio.h>
#include <string.h>
#include <stdint.h>

#ifndef _EBTE_H
#define _EBTE_H

#define debug_print(fmt, ...) \
        do { if (DEBUG) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); } while (0)

/* structural tokens */
static const uint8_t BEGIN_RESOLVE = '[';
static const uint8_t END_RESOLVE = ']';
static const uint8_t SPACE = 0x20;

/* work tokens */
static const uint8_t HASH = '#';
#define check_cons(value) (value >= 0x20 && value <= 0x7E)
#define check_char(value) ((value >= 0x41 && value <= 0x5A) || \
                           (value >= 0x61 && value <= 0x7A))

#define check_num(value) (value >= 0x30 && value <= 0x39)

#define EV_RESOLVED_NAME(prev, current) ((prev == RST_RESOLVE_STR && current == ST_RESOLVE) || \
                                         (prev == RST_RESOLVE_STR && current == ST_WORK) )


enum ebte_type {
    STRING = 0x1,
    NUMBER = 0x2
};

enum ebte_states {
    ST_REGULAR = 0x01,
    ST_WORK = 0x02,
    ST_END = 0x04,
    ST_ERROR = 0x08,
    ST_RESOLVE = 0x10,
    ST_ITERATING = 0x20
};

enum ebte_resolve_iterator {
    IST_RESOLVE_ITERATOR = 0x01,
    IST_LOOPING = 0x02,
    IST_RESOLVE_ = 0x04
};

enum ebte_resolve_states {
    RST_RESOLVE_STR = 0x11,
    RST_RESOLVE_NUM = 0x12
};

typedef enum ebte_type ebte_type;
typedef enum ebte_states ebte_states;

typedef struct _ebte_element {
    uint8_t *data;
    size_t size;
    ebte_type type;
} ebte_element;

#define state_return(function) return (void*) function

typedef void* (*state_process) (ebte_states *state, uint8_t e);
typedef void (*emit_cb) (uint8_t *stream, ebte_states prev,
                                          ebte_states current, int b, int e);
typedef int (*emit_filter) (ebte_states prev, ebte_states current);

state_process process_regular (ebte_states *state, uint8_t e);

state_process process_work (ebte_states *state, uint8_t e);

state_process process_resolve (ebte_states *state, uint8_t e);

state_process process_iterate (ebte_states *state, uint8_t e);

state_process process_resolve_str_cons (ebte_states *states, uint8_t e);

state_process process_resolve_str_number (ebte_states *states, uint8_t e);

ebte_states eval (uint8_t *stream, emit_filter, emit_cb emit);

#endif
