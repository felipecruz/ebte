#include <stdlib.h>
#include "ebte.h"

state_process process_regular (ebte_states *state, uint8_t e)
{
    if (e == '\0') {
        *state = ST_REGULAR | ST_END;
        state_return(NULL);
    } else if (e == BEGIN_RESOLVE) {
        *state = ST_WORK;
        state_return(process_work);
    } else if (e == END_RESOLVE) {
        *state = ST_ERROR;
        return NULL;
    } else if (e == SPACE) {
        *state = ST_REGULAR;
        state_return(process_regular);
    } else if (e >= 0x20 && e <= 0x7E) { // ascii only - we want to support utf-8
        *state = ST_REGULAR;
        state_return(process_regular);
    } else {
        *state = ST_ERROR;
        state_return(NULL);
    }

}

state_process process_work (ebte_states *state, uint8_t e)
{
    if (e == '\0') {
        *state = ST_ERROR;
        state_return(NULL);
    } else if (e == BEGIN_RESOLVE) {
        *state = ST_RESOLVE;
        state_return(process_resolve);
    } else if (e == END_RESOLVE) {
        *state = ST_REGULAR;
        state_return(process_regular);
    } else if (e == SPACE) {
        *state = ST_WORK;
        state_return(process_work);
    } else if (e >= 0x20 && e <= 0x7E) { // ascii only - we target utf-8 support
        *state = ST_WORK;
        state_return(process_work);
    } else if (e == HASH) {
        *state = ST_ITERATING;
        state_return(process_iterate);
    } else {
        *state = ST_ERROR;
        state_return(NULL);
    }
}

state_process process_resolve (ebte_states *state, uint8_t e)
{
    if (check_char(e)) {
        *state = RST_RESOLVE_STR;
        state_return(process_resolve_str_cons);
    } else if (e == SPACE) {
        *state = ST_RESOLVE;
        state_return(process_resolve);
    } else if (e == END_RESOLVE) {
        *state = ST_WORK;
        state_return(process_work);
    }

    *state = ST_ERROR;
    state_return(process_resolve);
}


state_process process_resolve_str_cons (ebte_states *state, uint8_t e)
{
    if (check_char(e)) {
        *state = RST_RESOLVE_STR;
        state_return(process_resolve_str_cons);
    } else if (e == SPACE) {
        *state = ST_RESOLVE;
        state_return(process_resolve);
    } else if (e == END_RESOLVE) {
        *state = ST_WORK;
        state_return(process_work);
    }

    *state = ST_ERROR;
    state_return(process_resolve);

}
state_process process_resolve_str_number (ebte_states *state, uint8_t e)
{
    state_return(NULL);

}

state_process process_iterate (ebte_states *state, uint8_t e)
{
    state_return(NULL);
}

void _emit_print (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    if (e == -1)
        return;

    uint8_t *data = malloc(sizeof(uint8_t) * ((e - b) + 1));
    memcpy(data, (stream + b), (e - b) + 1);
    data[(e - b) + 1] = '\0';
    debug_print("Token: %-10s  Begin %d End %d\n", data, b, e);
    free(data);
}

int _no_filter (ebte_states prev, ebte_states current)
{
    return 1;
}

ebte_states eval (uint8_t *stream, emit_filter filter, emit_cb emit)
{
    int pos = 0;
    int element_begin = 0;
    int element_end = -1;
    uint8_t *original = stream;
    uint8_t lex = *(stream);

    ebte_states state = ST_REGULAR;
    ebte_states last_state = ST_REGULAR;
    state_process stateFn = (state_process)&process_regular;

    if (emit == NULL)
        emit = &_emit_print;
    if (filter == NULL)
        filter = &_no_filter;

    do {
        last_state = state;
        stateFn = stateFn (&state, lex);

        if (state & ST_END)
            break;

        if (state != last_state) {
            element_end = pos;
            if (filter(last_state, state))
                emit (original, last_state, state, element_begin, element_end);
            element_begin = ++pos;
        } else{
            element_end = pos++;
        }

        lex = *(++stream);
    } while (lex);

    /* Emit last state
     * Unless it's equal to ST_REGULAR it's an invalid last state
     */
    if (!(state & ST_REGULAR)) {
        emit (original, ST_ERROR, ST_ERROR, element_begin, element_end);
        state = ST_ERROR;
    }

    return state;
}
