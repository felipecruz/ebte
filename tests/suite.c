#include "thc.h"
#include "test_ebte.h"

int main (int argc, const char *argv[])
{
    /* ebte tests */
    thc_addtest(test_eval_end_state);
    thc_addtest(test_resolve);
    thc_addtest(test_execute_only_resolve);

    return thc_run(THC_VERBOSE | THC_NOFORK);
}
