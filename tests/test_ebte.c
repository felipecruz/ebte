#include <stdlib.h>
#include <strings.h>
#include "ebte.h"
#include "thc.h"

void test_eval_end_state (void)
{
    uint8_t expr1[] = "";
    uint8_t expr2[] = "Value";
    uint8_t expr3[] = "[";
    uint8_t expr4[] = "[]";
    uint8_t expr5[] = "[val";
    uint8_t expr6[] = "[val]";
    uint8_t expr7[] = "Value[val]eulaV";
    uint8_t expr8[] = "Value[val";

    ENSURE (eval (expr1, NULL, NULL) & (ST_REGULAR | ST_END));
    ENSURE (eval (expr2, NULL, NULL) & ST_REGULAR);
    ENSURE (eval (expr3, NULL, NULL) & ST_ERROR);
    ENSURE (eval (expr4, NULL, NULL) & ST_REGULAR);
    ENSURE (eval (expr5, NULL, NULL) & ST_ERROR);
    ENSURE (eval (expr6, NULL, NULL) & ST_REGULAR);
    ENSURE (eval (expr7, NULL, NULL) & ST_REGULAR);
    ENSURE (eval (expr8, NULL, NULL) & ST_ERROR);
}

void test_emit_token1 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_WORK);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}

void test_emit_token2 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_WORK);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}

void test_emit_token3 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_RESOLVE);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}

void test_emit_token4 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_RESOLVE);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}


void test_emit_token5 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_WORK);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}


void test_emit_token6 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_RESOLVE);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}

void test_emit_token7 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_WORK);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}

void test_emit_token8 (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    static int c = 0;
    if (e == -1)
        return;

    if (c == 0) {
        ENSURE (prev == ST_REGULAR);
        ENSURE (current == ST_WORK);
    } else if (c == 1) {
        ENSURE (prev == ST_WORK);
        ENSURE (current == ST_RESOLVE);
    } else if (c == 2) {
        ENSURE (prev == ST_RESOLVE);
        ENSURE (current == RST_RESOLVE_STR);
    } else if (c == 3) {
        ENSURE (prev == RST_RESOLVE_STR);
        ENSURE (current == ST_RESOLVE);
        //TODO default begin is right shifted.. we need to back 1 position
        ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
    }
    ++c;
}

void test_resolve (void)
{
    uint8_t expr1[] = "[[work]]";
    uint8_t expr2[] = "[ [work] ]";
    uint8_t expr3[] = "[[ work ]]";
    uint8_t expr4[] = "[[   work   ]]";
    uint8_t expr5[] = "[   [work]   ]";
    uint8_t expr6[] = "[[work   ]   ]";
    uint8_t expr7[] = "[   [   work]]";
    uint8_t expr8[] = "[   [   work   ]   ]";

    ENSURE (eval (expr1, NULL, test_emit_token1) & ST_REGULAR);
    ENSURE (eval (expr2, NULL, test_emit_token2) & ST_REGULAR);
    ENSURE (eval (expr3, NULL, test_emit_token3) & ST_REGULAR);
    ENSURE (eval (expr4, NULL, test_emit_token4) & ST_REGULAR);
    ENSURE (eval (expr5, NULL, test_emit_token5) & ST_REGULAR);
    ENSURE (eval (expr6, NULL, test_emit_token6) & ST_REGULAR);
    ENSURE (eval (expr7, NULL, test_emit_token7) & ST_REGULAR);
    ENSURE (eval (expr8, NULL, test_emit_token8) & ST_REGULAR);
}

void test_filtered_emit (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    ENSURE (0 == strncmp ((stream + b - 1), "work", (e - b)));
}

int resolve_filter (ebte_states prev, ebte_states current)
{
    if (EV_RESOLVED_NAME(prev, current))
        return 1;
    return 0;
}

void test_execute_only_resolve (void)
{
    uint8_t expr1[] = "Teste [[work]]xx";
    uint8_t expr2[] = "  [ [work] ]";
    uint8_t expr3[] = "x [[ work ]]xx";
    uint8_t expr4[] = "x[[   work   ]]xx";
    uint8_t expr5[] = " x [   [work]   ]xx xx";
    uint8_t expr6[] = "xx [[work   ]   ]x";
    uint8_t expr7[] = "x[   [   work]]  xx";
    uint8_t expr8[] = "xxx [   [   work   ]   ]xxx xx x ";

    ENSURE (eval (expr1, resolve_filter, test_filtered_emit) & ST_REGULAR);
    ENSURE (eval (expr2, resolve_filter, test_filtered_emit) & ST_REGULAR);
    ENSURE (eval (expr3, resolve_filter, test_filtered_emit) & ST_REGULAR);
    ENSURE (eval (expr4, resolve_filter, test_filtered_emit) & ST_REGULAR);
    ENSURE (eval (expr5, resolve_filter, test_filtered_emit) & ST_REGULAR);
    ENSURE (eval (expr6, resolve_filter, test_filtered_emit) & ST_REGULAR);
    ENSURE (eval (expr7, resolve_filter, test_filtered_emit) & ST_REGULAR);
    ENSURE (eval (expr8, resolve_filter, test_filtered_emit) & ST_REGULAR);
}

