# Ebte - event based template engine

Inspired by Rob Pike's "state function loop" model (http://cuddle.googlecode.com/hg/talk/lex.html#slide-17),
Ebte is a Event Based Template Engine.

## API

```C
/*
 * uint8_t     *stream - data pointer
 * emit_filter filter  - filter function or NULL (no filter)
 * emit_cb     emit    - callback function or NULL (emit_print)
 *
 * return: ST_REGULAR - valid input
 *         ST_ERROR   - invalid input
 * /
ebte_states eval (uint8_t *stream, emit_filter filter, emit_cb emit);

typedef void (*emit_cb) (uint8_t *stream, ebte_states prev,
                                          ebte_states current, int b, int e);
typedef int (*emit_filter) (ebte_states prev, ebte_states current);
```

### Filter

Must return 1 if we want our `emit_cb` function to be called by this event or
0 to ignore current event.

### Emit Callback

This function is always called with a original input pointer, previous state,
current state and begin and end indexes.

A **event** is a pair of previous and current states.

```C
#define EV_RESOLVED_NAME(prev, current) ((prev == RST_RESOLVE_STR && current == ST_RESOLVE) || \
                                         (prev == RST_RESOLVE_STR && current == ST_WORK) )
```

## API Example

```C
void test_filtered_emit (uint8_t *stream, ebte_states prev,
                                  ebte_states current, int b, int e)
{
    uint8_t resolved_name[] = (stream + b - 1);
    resolved_name[(e - b) + 1] = '\0';

    /* resolved_name == "work" */
}

int resolve_filter (ebte_states prev, ebte_states current)
{
    if (EV_RESOLVED_NAME(prev, current))
        return 1;
    return 0;
}

uint8_t expr8[] = "<title>[[ work ]]</title>";
ebte_states = eval (expr1, resolve_filter, test_filtered_emit);
```

## Resolving?

This will be a call to `resolve (char *key, void *context);`. That means
you'll be able to implement resolve as you want and use your favorite underlying
implementation like a Python dict, Ruby hashes or maybe just a C hash table.

## TODO

1. Command line execution `rebte config.sh.tmpl config.sh server:www.ebte.org`
2. Define plugabble resolving mechanism
3. Iterators `[ # list_like ] ... [ ## ]`


## Tests

Tests

```sh
make test
```

Tests in debug mode (with debug messages)

```sh
make dtest
```

