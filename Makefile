GCC=gcc
SRC=src
TESTS=tests
INCLUDE=include
BUILD=build
FLAGS=-DDEBUG=0 -std=c99
DEBUG_FLAGS=-DDEBUG=1 -std=c99

SOURCES=$(SRC)/ebte.c
TEST_SOURCES=$(TESTS)/thc.c $(TESTS)/test_ebte.c $(TESTS)/suite.c

all:
	$(GCC) -I $(INCLUDE) -c $(SRC)/ebte.c
	ar rcs libebte.a ebte.o
clean:
	rm ebte.o libebte.a
	rm -rf build/
bin_dir:
	@mkdir -p $(BUILD)
test: bin_dir
	$(GCC) -I $(INCLUDE) $(SOURCES) $(TEST_SOURCES) $(FLAGS) -o $(BUILD)/etbe_bin && $(BUILD)/etbe_bin
dtest: bin_dir
	$(GCC) -I $(INCLUDE) $(SOURCES) $(TEST_SOURCES) $(DEBUG_FLAGS) -o $(BUILD)/etbe_bin && $(BUILD)/etbe_bin
debug: bin_dir
	$(GCC) -I $(INCLUDE) -g $(SOURCES) $(TEST_SOURCES) $(DEBUG_FLAGS) -o $(BUILD)/etbe_bin && gdb $(BUILD)/etbe_bin
leak: bin_dir
	$(GCC) -I $(INCLUDE) -g $(SOURCES) $(TEST_SOURCES) $(FLAGS) -o $(BUILD)/list && valgrind --show-reachable=yes --leak-check=full $(BUILD)/etbe_bin
